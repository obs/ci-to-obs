#!/usr/bin/env python3

import argparse
import logging
import pathlib
import re
import subprocess
import time
import urllib.parse

import git
import gitlab
import osc.conf
import osc.core
import tenacity

SCRATCH_REPO_DIR = "/tmp/scratchrepo"
TEST_BRANCH = "wip/test/fake"


def _debian_mangle_version_to_tag(v):
    # see https://dep-team.pages.debian.net/deps/dep14/
    v = v.replace(":", "%")
    v = v.replace("~", "_")
    v = re.sub("\\.(?=\\.|$|lock$)", ".#", v)
    return v

def _retrying_on_exception(exception_type):
    retrying = tenacity.Retrying(
        stop=tenacity.stop_after_delay(30),
        retry=tenacity.retry_if_exception_type(exception_type),
        wait=tenacity.wait_fixed(1),
    )
    return retrying

def _monitor_pipeline_for_completion(pipeline):
    logging.info(f"monitoring pipeline {pipeline.web_url}")
    while pipeline.status in ("running", "pending", "created", "waiting_for_resource"):
        logging.debug(f"pipeline {pipeline.web_url} status is {pipeline.status}")
        pipeline.refresh()
        time.sleep(5)


def _commit_and_push(gl, repo, filename, msg):
    repo.index.add(filename)
    actor = git.Actor(gl.user.name, gl.user.email)
    commit = repo.index.commit(msg, author=actor, committer=actor)
    logging.info(f"committed {commit.hexsha} to {TEST_BRANCH}")
    pushes = repo.remotes.origin.push(TEST_BRANCH, force=True, verbose=True)
    assert not any(p.flags & git.remote.PushInfo.ERROR for p in pushes)
    return commit

def _commit_tag_and_push(gl, repo, tag, filename, msg):
    commit = _commit_and_push(gl, repo, filename, msg)
    env = {
            "GIT_COMMITTER_NAME": gl.user.name,
            "GIT_COMMITTER_EMAIL": gl.user.email,
    }
    new_tag = repo.create_tag (tag, env=env, message=msg)
    pushes = repo.remotes.origin.push(new_tag, force=True, verbose=True)
    assert not any(p.flags & git.remote.PushInfo.ERROR for p in pushes)
    return commit


def _get_debian_version():
    cmd = subprocess.run(
        ["dpkg-parsechangelog", "-SVersion"],
        capture_output=True,
        check=True,
        cwd=SCRATCH_REPO_DIR,
    )
    return cmd.stdout.decode().strip()

class GitLabToOBSTester:
    def __init__(self, reference, scratch, testing_pipeline_url, builder_image):
        self.apiurl = None
        self.gl = None
        self.reference = reference
        self.package = reference.split("/")[-1]
        self.scratch = scratch
        self.scratch_git = None
        self.scratch_gitlab = None
        self.testing_pipeline_url = testing_pipeline_url
        self.builder_image = builder_image

    def connect(self, gitlab_instance, gitlab_server_url, gitlab_auth_token, oscrc):
        if gitlab_server_url:
            logging.info(f'Connecting to the "{gitlab_server_url}" instance')
            self.gl = gitlab.Gitlab(gitlab_server_url, private_token=gitlab_auth_token)
        else:
            logging.info(f'Connecting to the "{gitlab_instance}" configured instance')
            self.gl = gitlab.Gitlab.from_config(gitlab_instance)
        self.gl.auth()
        logging.info(f"Logged to GitLab as {self.gl.user.name} <{self.gl.user.email}>")
        osc.conf.get_config(override_conffile=oscrc)
        self.apiurl = osc.conf.config["apiurl"]

    def _scratch_set_properties(self):
        logging.info(f"setting up properties on {self.scratch}, temporarily disable CI")
        s = self.gl.projects.get(self.scratch)
        self.scratch_gitlab = s
        try:
            s.branches.delete(TEST_BRANCH)
        except gitlab.GitlabDeleteError:
            pass
        s.merge_requests_access_level = "enabled"
        s.merge_method = "ff"
        s.builds_access_level = "disabled"
        self.scratch_gitlab.ci_config_path = "nonexisting-gitlab-ci.yml"
        s.lfs_enabled = True
        s.save()

    def _scratch_clone(self):
        url = urllib.parse.urlparse(self.scratch_gitlab.http_url_to_repo)
        password = urllib.parse.quote(self.gl.private_token, safe="")
        netloc = f"oauth2:{password}@{url.netloc}"
        url = url._replace(netloc=netloc)
        logging.info(f"clone {url.geturl()} to {SCRATCH_REPO_DIR}")
        self.scratch_git = git.Repo.clone_from(url.geturl(), SCRATCH_REPO_DIR)

    def prepare_scratch(self):
        self._scratch_set_properties()
        self._scratch_clone()

    def _scratch_set_variables(self):
        obs_project = self._get_obs_test_project_prefix() + self.package
        logging.info(f"force OBS projects under {obs_project}")
        varname = "OBS_PROJECT"
        vardata = {"key": varname, "value": obs_project}
        try:
            self.scratch_gitlab.variables.update(varname, vardata)
        except gitlab.GitlabUpdateError:
            self.scratch_gitlab.variables.create(vardata)

        varname = "CI_BUILDER_IMAGE"
        vardata = {"key": varname, "value": self.builder_image}
        try:
            self.scratch_gitlab.variables.update(varname, vardata)
        except gitlab.GitlabUpdateError:
            self.scratch_gitlab.variables.create(vardata)

    def _scratch_cleanup_tags(self):
        scratch = self.scratch_git
        old_tags = list(scratch.tags)
        logging.debug(f"cleaning up the remote tags {old_tags}")
        if old_tags:
            pushes = scratch.remotes.origin.push(old_tags, delete=True, verbose=True)
            assert not any(p.flags & git.remote.PushInfo.ERROR for p in pushes)
            scratch.delete_tag(old_tags)

    def _scratch_synchronize_branches(self, reference):
        scratch = self.scratch_git
        default = reference.default_branch
        logging.debug("synchronizing branches")
        branches = [b.name for b in reference.branches.list(all=True)]
        logging.info(f"synchronizing the {', '.join(branches)} branches")

        for branchname in branches:
            scratch.create_head(
                branchname,
                commit=scratch.remotes.reference.refs[branchname],
                force=True,
            )
        logging.debug(
            f"pushing {branches} to {self.scratch_gitlab.path_with_namespace}"
        )
        pushes = scratch.remotes.origin.push(
            branches, force=True, verbose=True, follow_tags=True
        )
        assert not any(p.flags & git.remote.PushInfo.ERROR for p in pushes)

    def reset_scratch(self):
        scratch = self.scratch_git
        logging.info(
            f"resetting {self.scratch_gitlab.path_with_namespace} to {self.reference}"
        )
        self._scratch_cleanup_tags()
        reference = self.gl.projects.get(self.reference)
        default = reference.default_branch
        url = reference.http_url_to_repo
        scratch.create_remote("reference", url)
        scratch.remotes.reference.fetch()
        head = scratch.remotes.reference.refs[default]
        logging.debug(f"switching to the {TEST_BRANCH} branch as {head.commit.hexsha}")
        scratch.create_head(TEST_BRANCH, commit=head.commit).checkout()
        self._scratch_synchronize_branches(reference)
        logging.debug(
            f"setting the default branch on {self.scratch_gitlab.path_with_namespace} to {default} and re-enable CI"
        )
        self.scratch_gitlab.default_branch = default
        self.scratch_gitlab.builds_access_level = "enabled"
        self.scratch_gitlab.save()
        self._scratch_set_variables()

    def _get_obs_home_branches_prefix(self):
        username = osc.conf.config["api_host_options"][self.apiurl]["user"]
        prefix = f"home:{username}:branches:"
        return prefix

    def _get_obs_reference_project_name(self):
        return self._get_obs_home_branches_prefix() + self.package

    def _get_obs_test_project_prefix(self):
        return self._get_obs_home_branches_prefix() + "test:"

    def _get_obs_test_project_name(self):
        return self._get_obs_test_project_prefix() + self.package

    def obs_prepare_work_areas(self):
        logging.info("preparing work areas on OBS")
        package = self.reference.split("/")[-1]
        src_project = self._get_obs_reference_project_name()
        target_project = self._get_obs_test_project_name()
        msg = "Branch for testing the GitLab-to-OBS pipeline"
        for suffix in ("", ":snapshots"):
            logging.info("branching %s from %s to %s" % (package, src_project, target_project + suffix))
            osc.core.branch_pkg(
                self.apiurl,
                src_project,
                package,
                target_project=target_project + suffix,
                msg=msg,
                force=True,
            )

    def point_gitlab_ci_here(self, ci_config_path):
        logging.info(f"pointing to the CI definition at {ci_config_path}")
        self.scratch_gitlab.ci_config_path = ci_config_path
        self.scratch_gitlab.save()

    def test_nonrelease_mr(self):
        logging.info(f"testing an unreleased version on {TEST_BRANCH}")
        assert self.scratch_git.active_branch.name == TEST_BRANCH
        subprocess.run(
            ["dch", "-i", "Fake changes while testing the GitLab-to-OBS pipeline"],
            check=True,
            cwd=SCRATCH_REPO_DIR,
        )
        msg = f"Test unreleased changes\n\nPipeline: {self.testing_pipeline_url}"
        commit = _commit_and_push(self.gl, self.scratch_git, "debian/changelog", msg)
        for attempt in _retrying_on_exception(IndexError):
            # give some time to GitLab to kick the pipeline when it detects new commits
            with attempt:
                pipeline = self.scratch_gitlab.pipelines.list(
                    ref=TEST_BRANCH, sha=commit.hexsha
                )[0]
        _monitor_pipeline_for_completion(pipeline)
        assert (
            pipeline.status == "success"
        ), f"submitted non-release pipeline {pipeline.web_url} didn't succeed: {pipeline.status}"
        job_names = {j.name for j in pipeline.jobs.list()}
        assert job_names == {
            "build debian source",
        }, f"submitted non-release pipeline on unmerged changes expected different jobs: {job_names}"
        target = self.scratch_gitlab.default_branch
        mr = self.scratch_gitlab.mergerequests.create(
            dict(
                source_branch=TEST_BRANCH,
                target_branch=target,
                title="Test non-release",
            )
        )
        logging.info(f"created non-release MR {mr.web_url}")
        pipelines = [p for p in mr.pipelines() if p["sha"] == commit.hexsha]
        assert (
            len(pipelines) == 1
        ), f"non-release MR has unexpected pipelines: {' '.join(p['web_url'] for p in pipelines)}"
        assert mr.pipelines()[0]["id"] == pipeline.id
        logging.info(f"landing {mr.web_url}")
        for attempt in _retrying_on_exception(gitlab.exceptions.GitlabMRClosedError):
            # sometimes GitLab need some time to compute the MR status correctly
            with attempt:
                mr.merge()
        for attempt in _retrying_on_exception(IndexError):
            # give some time to GitLab to kick the pipeline when it detects new commits
            with attempt:
                pipeline = self.scratch_gitlab.pipelines.list(
                    ref=self.scratch_gitlab.default_branch, sha=commit.hexsha
                )[0]
        _monitor_pipeline_for_completion(pipeline)
        assert (
            pipeline.status == "success"
        ), f"landed non-release pipeline {pipeline.web_url} didn't succeed: {pipeline.status}"
        job_names = {j.name for j in pipeline.jobs.list()}
        assert job_names == {
            "build debian source",
            "upload to obs",
        }, f"landed non-release pipeline expected different jobs: {job_names}"
        logging.info("Non-release MR landed successfully ✅")

    def test_release_commit(self):
        logging.info("submitting a tagged release commit")
        subprocess.run(["dch", "-r", "apertis"], check=True, cwd=SCRATCH_REPO_DIR)
        version = _get_debian_version();
        msg = f"Test release commit\n\nPipeline: {self.testing_pipeline_url}"
        commit = _commit_tag_and_push(self.gl, self.scratch_git, f"debian/{version}", "debian/changelog", msg)
        for attempt in _retrying_on_exception(IndexError):
            # give some time to GitLab to kick the pipeline when it detects new commits
            with attempt:
                pipeline = self.scratch_gitlab.pipelines.list(
                    ref=TEST_BRANCH, sha=commit.hexsha
                )[0]
        _monitor_pipeline_for_completion(pipeline)
        assert (
            pipeline.status == "success"
        ), f"submitted release pipeline {pipeline.web_url} didn't succeed: '{pipeline.status}'"
        job_names = {j.name for j in pipeline.jobs.list()}
        assert job_names == {
            "build debian source",
        }, f"submitted release pipeline on unmerged changes expected different jobs: {job_names}"
        return commit

    def test_release_mr_to_default_branch(self, commit):
        logging.info("testing MR to default branch")
        target = self.scratch_gitlab.default_branch
        mr = self.scratch_gitlab.mergerequests.create(
            dict(source_branch=TEST_BRANCH, target_branch=target, title="Test release")
        )
        logging.info(f"created release MR {mr.web_url} to default branch {target}")
        pipelines = [p for p in mr.pipelines() if p["sha"] == commit.hexsha]
        assert len(pipelines) == 1
        assert pipelines[0]["ref"] == TEST_BRANCH
        logging.info(f"landing {mr.web_url}")
        for attempt in _retrying_on_exception(gitlab.exceptions.GitlabMRClosedError):
            # sometimes GitLab need some time to compute the MR status correctly
            with attempt:
                mr.merge()
        for attempt in _retrying_on_exception(IndexError):
            # give some time to GitLab to kick the pipeline when it detects new commits
            with attempt:
                pipeline = self.scratch_gitlab.pipelines.list(
                    ref=self.scratch_gitlab.default_branch, sha=commit.hexsha
                )[0]
        _monitor_pipeline_for_completion(pipeline)
        assert (
            pipeline.status == "success"
        ), f"landed release pipeline {pipeline.web_url} didn't succeed: '{pipeline.status}'"
        job_names = {j.name for j in pipeline.jobs.list()}
        assert job_names == {
            "build debian source",
            "upload to obs",
        }, f"landed release pipeline on merged changes expected different jobs: {job_names}"
        logging.info("Release MR landed successfully ✅")

    def test_release_artifacts(self, commit):
        version = _get_debian_version()
        project = self._get_obs_test_project_name()
        files = osc.core.meta_get_filelist(self.apiurl, project, self.package)
        dscs = [f for f in files if f.endswith(".dsc")]
        assert len(dscs) == 1, "expected a single .dsc, got {dscs}"
        dsc = dscs[0]
        expected = f"{self.package}_{version}.dsc"
        assert dsc == expected, f"expected '{expected}' on OBS, got '{dsc}'"
        logging.info(f"Found {dsc} succesfully ✅")


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Test the GitLab-to-OBS pipeline")
    parser.add_argument(
        "--debug",
        action="store_const",
        dest="loglevel",
        const=logging.DEBUG,
        help="print debug information",
    )
    parser.add_argument(
        "--scratch-repository",
        type=str,
        required=True,
        help="the repository on which the pipeline is tested",
    )
    parser.add_argument(
        "--reference-repository",
        type=str,
        required=True,
        help="the scratch repository will be reset to the reference-repository contents",
    )
    parser.add_argument(
        "--gitlab-instance",
        type=str,
        default="apertis",
        help="get connection parameters from this configured instance",
    )
    parser.add_argument(
        "--gitlab-auth-token", type=str, help="the GitLab authentication token"
    )
    parser.add_argument("--gitlab-server-url", type=str, help="the GitLab instance URL")
    parser.add_argument(
        "--oscrc", type=str, help="the OSC configuration with the OBS credentials"
    )
    parser.add_argument(
        "--testing-pipeline-url",
        type=str,
        help="The URL of the pipeline running the test",
    )
    parser.add_argument(
        "--ci-config-path",
        type=str,
        required=True,
        help="The path to the CI config to test",
    )
    parser.add_argument(
        "--builder-image",
        type=str,
        required=True,
        help="The name of the builder image to test",
    )
    args = parser.parse_args()

    logging.basicConfig(level=args.loglevel or logging.INFO)

    t = GitLabToOBSTester(
        reference=args.reference_repository,
        scratch=args.scratch_repository,
        testing_pipeline_url=args.testing_pipeline_url,
        builder_image=args.builder_image,
    )
    t.connect(
        args.gitlab_instance, args.gitlab_server_url, args.gitlab_auth_token, args.oscrc
    )
    t.prepare_scratch()
    t.reset_scratch()
    t.obs_prepare_work_areas()
    t.point_gitlab_ci_here(args.ci_config_path)
    t.test_nonrelease_mr()
    release_commit = t.test_release_commit()
    t.test_release_mr_to_default_branch(release_commit)
    t.test_release_artifacts(release_commit)
    logging.info("Test completed successfully ✨")
